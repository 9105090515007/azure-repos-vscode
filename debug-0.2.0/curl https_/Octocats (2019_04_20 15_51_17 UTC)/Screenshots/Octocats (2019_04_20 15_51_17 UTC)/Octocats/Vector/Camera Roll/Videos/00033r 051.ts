export declare enum EventName {
    ERROR = "error",
    INFO = "info",
    OPERATION_START = "opStart",
    OPERATION_END = "opEnd"
}
export declare namespace ErrorCodes {
    const NO_ERROR: number;
    const GENERAL_ERROR: number;
}
export declare enum ErrorType {
    USER_ERROR = "userError",
    SYSTEM_ERROR = "systemError"
}
export interface TelemetryEvent {
    eventName: EventName;
}
export interface Operation {
    operationId: string;
    operationName: string;
}
export interface ErrorInfo {
    errorCode: number;
    errorType?: ErrorType;
    message?: string;
    stack?: string;
}
export interface OperationStartEvent extends TelemetryEvent, Operation {
}
export interface OperationEndEvent extends TelemetryEvent, Operation, ErrorInfo {
    duration: number;
}
export interface OperationErrorEvent extends TelemetryEvent, Operation, ErrorInfo {
}
export interface ErrorEvent extends TelemetryEvent, ErrorInfo {
}
export declare const DimensionEntries: string[];
export declare const MeasurementEntries: string[];
