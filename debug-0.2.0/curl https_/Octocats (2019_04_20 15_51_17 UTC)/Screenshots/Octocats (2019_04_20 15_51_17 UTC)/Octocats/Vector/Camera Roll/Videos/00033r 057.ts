/**
 * Initialize TelemetryReporter by parsing attributes from a JSON file.
 * It reads these attributes: publisher, name, version, aiKey.
 * @param jsonFilepath absolute path of a JSON file.
 * @param debug If set as true, debug information be printed to console.
 */
export declare function initializeFromJsonFile(jsonFilepath: string, debug?: boolean): Promise<void>;
/**
 * Initialize TelemetryReporter from given attributes.
 * @param extensionId Identifier of the extension, used as prefix of EventName in telemetry data.
 * @param version Version of the extension.
 * @param aiKey Key of Application Insights.
 * @param debug If set as true, debug information be printed to console.
 */
export declare function initialize(extensionId: string, version: string, aiKey: string, debug?: boolean): void;
/**
 * Mark an Error instance as a user error.
 */
export declare function setUserError(err: Error): void;
/**
 * Set custom error code or an Error instance.
 * @param errorCode A custom error code.
 */
export declare function setErrorCode(err: Error, errorCode: number): void;
/**
 * Instrument callback for a command to auto send OPEARTION_START, OPERATION_END, ERROR telemetry.
 * @param operationName For extension activation, use "activation", for VS Code commands, use command name.
 * @param cb The callback function with a unique Id passed by its 1st parameter.
 * @returns The instrumented callback.
 */
export declare function instrumentOperation(operationName: string, cb: (operationId: string, ...args: any[]) => any): (...args: any[]) => any;
/**
 * Send OPERATION_START event.
 * @param operationId Unique id of the operation.
 * @param operationName Name of the operation.
 */
export declare function sendOperationStart(operationId: string, operationName: string): void;
/**
 * Send OPERATION_END event.
 * @param operationId Unique id of the operation.
 * @param operationName Name of the operation.
 * @param duration Time elapsed for the operation, in milliseconds.
 * @param err An optional Error instance if occurs during the operation.
 */
export declare function sendOperationEnd(operationId: string, operationName: string, duration: number, err?: Error): void;
/**
 * Send an ERROR event.
 * @param err An Error instance.
 */
export declare function sendError(err: Error): void;
/**
 * Send an ERROR event during an operation, carrying id and name of the oepration.
 * @param operationId Unique id of the operation.
 * @param operationName Name of the operation.
 * @param err An Error instance containing details.
 */
export declare function sendOperationError(operationId: string, operationName: string, err: Error): void;
/**
 * Create a UUID string using uuid.v4().
 */
export declare function createUuid(): string;
