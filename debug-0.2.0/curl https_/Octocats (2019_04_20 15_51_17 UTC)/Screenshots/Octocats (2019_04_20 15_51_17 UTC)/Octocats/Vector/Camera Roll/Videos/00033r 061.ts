export enum LogLevel {
    UNDEFINED = 0,
    FATAL = 100,
    ERROR = 200,
    WARN = 300,
    INFO = 400,
    VERBOSE = 500,
}
