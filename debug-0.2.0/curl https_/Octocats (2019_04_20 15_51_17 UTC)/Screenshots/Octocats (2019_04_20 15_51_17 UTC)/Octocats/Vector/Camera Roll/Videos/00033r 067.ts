import { ICustomEvent } from "./Interfaces";
export declare class Session {
    id: string;
    action: string;
    exitCode?: string;
    startAt: Date;
    stopAt?: Date;
    extraProperties: {
        [key: string]: any;
    };
    extraMeasures: {
        [key: string]: any;
    };
    constructor(action: string);
    getCustomEvent(): ICustomEvent;
    end(): void;
}
