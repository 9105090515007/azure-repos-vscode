import * as vscode from "vscode";
import TelemetryReporter from "vscode-extension-telemetry";
import { Session } from "./Session";
export declare namespace TelemetryWrapper {
    function initilizeFromJsonFile(fsPath: string): Promise<void>;
    function initilize(publisher: string, name: string, version: string, aiKey: string): void;
    function registerCommand(command: string, callback: (...args: any[]) => any): vscode.Disposable;
    function getReporter(): TelemetryReporter;
    function startSession(name: string): Session;
    function endSession(session: Session): void;
    function currentSession(): any;
    /**
     * Send a telemetry record with event name "fatal".
     * @param message a string or a JSON string.
     * @param exitCode
     */
    function fatal(message: string, exitCode?: string): void;
    /**
     * Send a telemetry record with event name "error".
     * @param message a string or a JSON string.
     * @param exitCode
     */
    function error(message: string, exitCode?: string): void;
    /**
     * Send a telemetry record with event name "info".
     * @param message a string or a JSON string.
     * @param exitCode
     */
    function info(message: string): void;
    /**
     * Send a telemetry record with event name "warn".
     * @param message a string or a JSON string.
     * @param exitCode
     */
    function warn(message: string): void;
    /**
     * Send a telemetry record with event name "verbose".
     * @param message a string or a JSON string.
     * @param exitCode
     */
    function verbose(message: string): void;
    function sendTelemetryEvent(eventName: string, properties?: {
        [key: string]: string;
    }, measures?: {
        [key: string]: number;
    }): void;
    enum EventType {
        ACTIVATION = "activation",
        FATAL = "fatal",
        ERROR = "error",
        WARN = "warn",
        INFO = "info",
        VERBOSE = "verbose",
        COMMAND_START = "commandStart",
        COMMAND_END = "commandEnd"
    }
}
