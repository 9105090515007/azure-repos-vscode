declare const _default: {
    requestContextHeader: string;
    requestContextSourceKey: string;
    requestContextTargetKey: string;
    requestContextSourceRoleNameKey: string;
    requestContextTargetRoleNameKey: string;
    requestIdHeader: string;
    parentIdHeader: string;
    rootIdHeader: string;
    correlationContextHeader: string;
};
export = _default;
