export interface XHROptions {
    type?: string;
    url?: string;
    user?: string;
    password?: string;
    headers?: any;
    timeout?: number;
    data?: any;
    agent?: any;
    strictSSL?: boolean;
    responseType?: string;
    followRedirects?: number;
}
export interface XHRResponse {
    responseText: string;
    status: number;
}
export interface XHRRequest {
    (options: XHROptions): Promise<XHRResponse>;
}
export declare function configure(_proxyUrl: string, _strictSSL: boolean): void;
export declare function xhr(options: XHROptions): Promise<XHRResponse>;
export declare function getErrorStatusDescription(status: number): string;
