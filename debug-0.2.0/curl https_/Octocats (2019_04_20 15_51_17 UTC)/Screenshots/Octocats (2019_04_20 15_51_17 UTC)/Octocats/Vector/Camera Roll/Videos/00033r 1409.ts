export declare function filterAsync<T>(array: T[], callback: (value: T, index: number) => Promise<boolean>): Promise<T[]>;
