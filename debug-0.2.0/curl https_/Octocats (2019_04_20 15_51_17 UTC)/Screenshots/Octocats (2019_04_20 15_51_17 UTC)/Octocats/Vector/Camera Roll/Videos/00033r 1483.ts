/// <reference types="node" />
import http = require("http");
import TelemetryClient = require("../Library/TelemetryClient");
import HttpRequestParser = require("./HttpRequestParser");
declare class AutoCollectHttpRequests {
    static INSTANCE: AutoCollectHttpRequests;
    private _client;
    private _isEnabled;
    private _isInitialized;
    private _isAutoCorrelating;
    constructor(client: TelemetryClient);
    enable(isEnabled: boolean): void;
    useAutoCorrelation(isEnabled: boolean): void;
    isInitialized(): boolean;
    isAutoCorrelating(): boolean;
    private _generateCorrelationContext(requestParser);
    private _initialize();
    /**
     * Tracks a request synchronously (doesn't wait for response 'finish' event)
     */
    static trackRequestSync(client: TelemetryClient, request: http.ServerRequest, response: http.ServerResponse, ellapsedMilliseconds?: number, properties?: {
        [key: string]: string;
    }, error?: any): void;
    /**
     * Tracks a request by listening to the response 'finish' event
     */
    static trackRequest(client: TelemetryClient, request: http.ServerRequest, response: http.ServerResponse, properties?: {
        [key: string]: string;
    }, _requestParser?: HttpRequestParser): void;
    /**
     * Add the target correlationId to the response headers, if not already provided.
     */
    private static addResponseCorrelationIdHeader(client, response);
    private static endRequest(client, requestParser, request, response, ellapsedMilliseconds?, properties?, error?);
    dispose(): void;
}
export = AutoCollectHttpRequests;
