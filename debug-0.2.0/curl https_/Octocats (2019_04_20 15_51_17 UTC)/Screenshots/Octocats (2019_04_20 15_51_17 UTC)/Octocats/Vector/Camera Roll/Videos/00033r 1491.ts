import { StaticFeature, BaseLanguageClient } from './client';
import { ClientCapabilities, Proposed } from 'vscode-languageserver-protocol';
export interface ConfigurationMiddleware {
    workspace?: {
        configuration?: Proposed.ConfigurationRequest.MiddlewareSignature;
    };
}
export declare class ConfigurationFeature implements StaticFeature {
    private _client;
    constructor(_client: BaseLanguageClient);
    fillClientCapabilities(capabilities: ClientCapabilities): void;
    initialize(): void;
    private getConfiguration(resource, section);
    private getConfigurationMiddleware();
}
