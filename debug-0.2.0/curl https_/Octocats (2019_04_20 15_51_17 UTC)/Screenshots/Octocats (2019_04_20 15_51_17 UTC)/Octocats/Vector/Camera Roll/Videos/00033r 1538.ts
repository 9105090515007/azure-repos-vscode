import { OutputEvent } from './debugSession';
export declare enum LogLevel {
    Verbose = 0,
    Log = 1,
    Warn = 2,
    Error = 3,
    Stop = 4,
}
export declare type ILogCallback = (outputEvent: OutputEvent) => void;
export interface ILogger {
    log(msg: string, level?: LogLevel): void;
    verbose(msg: string): void;
    warn(msg: string): void;
    error(msg: string): void;
}
export declare class Logger {
    private _currentLogger;
    private _pendingLogQ;
    log(msg: string, level?: LogLevel): void;
    verbose(msg: string): void;
    warn(msg: string): void;
    error(msg: string): void;
    /**
     * `log` adds a newline, `write` doesn't
     */
    private _write(msg, level?);
    /**
     * Set the logger's minimum level to log in the console, and whether to log to the file. Log messages are queued before this is
     * called the first time, because minLogLevel defaults to Warn.
     */
    setup(consoleMinLogLevel: LogLevel, logToFile: boolean): void;
    init(logCallback: ILogCallback, logFilePath?: string, logToConsole?: boolean): void;
}
export declare const logger: Logger;
export declare class LogOutputEvent extends OutputEvent {
    constructor(msg: string, level: LogLevel);
}
export declare function trimLastNewline(str: string): string;
