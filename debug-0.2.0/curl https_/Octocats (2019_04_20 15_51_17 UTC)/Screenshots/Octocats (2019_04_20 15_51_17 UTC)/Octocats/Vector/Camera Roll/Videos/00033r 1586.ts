/// <reference types="node" />
import Config = require("./Config");
declare class Sender {
    private static TAG;
    static WAIT_BETWEEN_RESEND: number;
    static TEMPDIR_PREFIX: string;
    private _config;
    private _storageDirectory;
    private _onSuccess;
    private _onError;
    private _enableDiskRetryMode;
    protected _resendInterval: number;
    constructor(config: Config, onSuccess?: (response: string) => void, onError?: (error: Error) => void);
    /**
    * Enable or disable offline mode
    */
    setDiskRetryMode(value: boolean, resendInterval?: number): void;
    send(payload: Buffer, callback?: (v: string) => void): void;
    saveOnCrash(payload: string): void;
    private _confirmDirExists(directory, callback);
    /**
     * Stores the payload as a json file on disk in the temp directory
     */
    private _storeToDisk(payload);
    /**
     * Stores the payload as a json file on disk using sync file operations
     * this is used when storing data before crashes
     */
    private _storeToDiskSync(payload);
    /**
     * Check for temp telemetry files
     * reads the first file if exist, deletes it and tries to send its load
     */
    private _sendFirstFileOnDisk();
    private _onErrorHelper(error);
}
export = Sender;
