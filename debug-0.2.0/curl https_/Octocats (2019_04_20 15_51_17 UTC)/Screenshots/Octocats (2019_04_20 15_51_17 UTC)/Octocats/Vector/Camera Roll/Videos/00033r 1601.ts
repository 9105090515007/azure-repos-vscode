import { IModulePatcher } from "diagnostic-channel";
export interface IWinstonData {
    message: string;
    meta: any;
    level: string;
    levelKind: string;
}
export declare const winston: IModulePatcher;
export declare function enable(): void;
