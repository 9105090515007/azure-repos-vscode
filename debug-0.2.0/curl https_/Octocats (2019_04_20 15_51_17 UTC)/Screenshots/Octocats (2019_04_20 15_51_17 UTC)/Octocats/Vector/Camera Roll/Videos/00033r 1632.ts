import { Proposed } from 'vscode-languageserver-protocol';
import { WorkspaceFeature } from './main';
export interface Configuration {
    getConfiguration(): Thenable<any>;
    getConfiguration(section: string): Thenable<any>;
    getConfiguration(item: Proposed.ConfigurationItem): Thenable<any>;
    getConfiguration(items: Proposed.ConfigurationItem[]): Thenable<any[]>;
}
export declare const ConfigurationFeature: WorkspaceFeature<Configuration>;
