/// <reference types="node" />
import cp = require('child_process');
export interface IForkOptions {
    cwd?: string;
    env?: any;
    encoding?: string;
    execArgv?: string[];
}
export declare function fork(modulePath: string, args: string[], options: IForkOptions, callback: (error: any, cp: cp.ChildProcess | undefined) => void): void;
