import { RequestType, RequestHandler, HandlerResult, CancellationToken } from 'vscode-jsonrpc';
export interface ConfigurationClientCapabilities {
    /**
     * The workspace client capabilities
     */
    workspace: {
        /**
        * The client supports `workspace/configuration` requests.
        */
        configuration?: boolean;
    };
}
/**
 * The 'workspace/configuration' request is sent from the server to the client to fetch a certain
 * configuration setting.
 */
export declare namespace ConfigurationRequest {
    const type: RequestType<ConfigurationParams, any[], void, void>;
    type HandlerSignature = RequestHandler<ConfigurationParams, any[], void>;
    type MiddlewareSignature = (params: ConfigurationParams, token: CancellationToken, next: HandlerSignature) => HandlerResult<any[], void>;
}
export interface ConfigurationItem {
    /**
     * The scope to get the configuration section for.
     */
    scopeUri?: string;
    /**
     * The configuration section asked for.
     */
    section?: string;
}
/**
 * The parameters of a configuration request.
 */
export interface ConfigurationParams {
    items: ConfigurationItem[];
}
