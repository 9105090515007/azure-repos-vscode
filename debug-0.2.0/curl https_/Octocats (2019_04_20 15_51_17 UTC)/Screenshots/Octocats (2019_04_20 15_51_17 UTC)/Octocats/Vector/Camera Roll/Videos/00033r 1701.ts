import { Event, Proposed } from 'vscode-languageserver-protocol';
import { WorkspaceFeature } from './main';
export interface WorkspaceFolders {
    getWorkspaceFolders(): Thenable<Proposed.WorkspaceFolder[] | null>;
    onDidChangeWorkspaceFolders: Event<Proposed.WorkspaceFoldersChangeEvent>;
}
export declare const WorkspaceFoldersFeature: WorkspaceFeature<WorkspaceFolders>;
