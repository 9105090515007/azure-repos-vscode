/// <reference types="node" />
import { DebugProtocol } from 'vscode-debugprotocol';
import { DebugSession } from './debugSession';
export declare class LoggingDebugSession extends DebugSession {
    private obsolete_logFilePath;
    constructor(obsolete_logFilePath: string, obsolete_debuggerLinesAndColumnsStartAt1?: boolean, obsolete_isServer?: boolean);
    start(inStream: NodeJS.ReadableStream, outStream: NodeJS.WritableStream): void;
    /**
     * Overload sendEvent to log
     */
    sendEvent(event: DebugProtocol.Event): void;
    /**
     * Overload sendRequest to log
     */
    sendRequest(command: string, args: any, timeout: number, cb: (response: DebugProtocol.Response) => void): void;
    /**
     * Overload sendResponse to log
     */
    sendResponse(response: DebugProtocol.Response): void;
    protected dispatchRequest(request: DebugProtocol.Request): void;
}
