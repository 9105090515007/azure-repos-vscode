/// <reference types="node" />
import * as ee from 'events';
import { DebugProtocol } from 'vscode-debugprotocol';
export declare class ProtocolServer extends ee.EventEmitter {
    private static TWO_CRLF;
    private _rawData;
    private _contentLength;
    private _sequence;
    private _writableStream;
    private _pendingRequests;
    constructor();
    start(inStream: NodeJS.ReadableStream, outStream: NodeJS.WritableStream): void;
    stop(): void;
    sendEvent(event: DebugProtocol.Event): void;
    sendResponse(response: DebugProtocol.Response): void;
    sendRequest(command: string, args: any, timeout: number, cb: (response: DebugProtocol.Response) => void): void;
    protected dispatchRequest(request: DebugProtocol.Request): void;
    private _emitEvent(event);
    private _send(typ, message);
    private _handleData(data);
}
