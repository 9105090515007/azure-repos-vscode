/**
 * Type of the metric data measurement.
 */
declare enum DataPointType {
    Measurement = 0,
    Aggregation = 1,
}
export = DataPointType;
