declare class Config {
    static ENV_azurePrefix: string;
    static ENV_iKey: string;
    static legacy_ENV_iKey: string;
    static ENV_profileQueryEndpoint: string;
    /** An identifier for your Application Insights resource */
    instrumentationKey: string;
    /** The id for cross-component correlation. READ ONLY. */
    correlationId: string;
    /** The ingestion endpoint to send telemetry payloads to */
    endpointUrl: string;
    /** The maximum number of telemetry items to include in a payload to the ingestion endpoint (Default 250) */
    maxBatchSize: number;
    /** The maximum amount of time to wait for a payload to reach maxBatchSize (Default 15000) */
    maxBatchIntervalMs: number;
    /** A flag indicating if telemetry transmission is disabled (Default false) */
    disableAppInsights: boolean;
    /** The percentage of telemetry items tracked that should be transmitted (Default 100) */
    samplingPercentage: number;
    /** The time to wait before retrying to retrieve the id for cross-component correlation (Default 30000) */
    correlationIdRetryIntervalMs: number;
    /** A list of domains to exclude from cross-component header injection */
    correlationHeaderExcludedDomains: string[];
    private endpointBase;
    private setCorrelationId;
    private _profileQueryEndpoint;
    constructor(instrumentationKey?: string);
    profileQueryEndpoint: string;
    private static _getInstrumentationKey();
}
export = Config;
