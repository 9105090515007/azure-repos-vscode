/// <reference types="node" />
import http = require("http");
import TelemetryClient = require("../Library/TelemetryClient");
declare class Util {
    static MAX_PROPERTY_LENGTH: number;
    private static document;
    /**
     * helper method to access userId and sessionId cookie
     */
    static getCookie(name: string, cookie: string): string;
    /**
     * helper method to trim strings (IE8 does not implement String.prototype.trim)
     */
    static trim(str: string): string;
    /**
     * Convert an array of int32 to Base64 (no '==' at the end).
     * MSB first.
     */
    static int32ArrayToBase64(array: number[]): string;
    /**
     * generate a random 32bit number (-0x80000000..0x7FFFFFFF).
     */
    static random32(): number;
    /**
     * generate a random 32bit number (0x00000000..0xFFFFFFFF).
     */
    static randomu32(): number;
    /**
     * generate GUID
     */
    static newGuid(): string;
    /**
     * Check if an object is of type Array
     */
    static isArray(obj: any): boolean;
    /**
     * Check if an object is of type Error
     */
    static isError(obj: any): boolean;
    /**
     * Check if an object is of type Date
     */
    static isDate(obj: any): boolean;
    /**
     * Convert ms to c# time span format
     */
    static msToTimeSpan(totalms: number): string;
    /**
     * Validate that an object is of type { [key: string]: string }
     */
    static validateStringMap(obj: any): {
        [key: string]: string;
    };
    /**
     * Checks if a request url is not on a excluded domain list
     * and if it is safe to add correlation headers
     */
    static canIncludeCorrelationHeader(client: TelemetryClient, requestUrl: string): boolean;
    static getCorrelationContextTarget(response: http.ClientResponse | http.ServerRequest, key: string): any;
}
export = Util;
