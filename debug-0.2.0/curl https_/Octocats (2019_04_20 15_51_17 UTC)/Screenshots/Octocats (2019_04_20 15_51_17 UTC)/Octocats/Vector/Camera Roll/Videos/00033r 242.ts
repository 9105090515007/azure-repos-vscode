/**
 * Defines the level of severity for the event.
 */
declare enum SeverityLevel {
    Verbose = 0,
    Information = 1,
    Warning = 2,
    Error = 3,
    Critical = 4,
}
export = SeverityLevel;
