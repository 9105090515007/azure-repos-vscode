/// <reference types="node" />
import * as stream from "stream";
import { TransformedStringTypeKind } from "../Type";
import { DateTimeRecognizer } from "../DateTime";
export declare enum Tag {
    Null = 0,
    False = 1,
    True = 2,
    Integer = 3,
    Double = 4,
    InternedString = 5,
    UninternedString = 6,
    Object = 7,
    Array = 8,
    StringFormat = 9,
    TransformedString = 10
}
export declare type Value = number;
export declare function valueTag(v: Value): Tag;
export declare class CompressedJSON {
    readonly dateTimeRecognizer: DateTimeRecognizer;
    private _rootValue;
    private _ctx;
    private _contextStack;
    private _strings;
    private _stringIndexes;
    private _objects;
    private _arrays;
    constructor(dateTimeRecognizer: DateTimeRecognizer);
    readFromStream(readStream: stream.Readable): Promise<Value>;
    getStringForValue(v: Value): string;
    getObjectForValue: (v: number) => number[];
    getArrayForValue: (v: number) => number[];
    getStringFormatTypeKind(v: Value): TransformedStringTypeKind;
    private internString;
    private makeString;
    private internObject;
    private internArray;
    private commitValue;
    private finish;
    private pushContext;
    private popContext;
    protected handleStartObject: () => void;
    protected handleEndObject: () => void;
    protected handleStartArray: () => void;
    protected handleEndArray: () => void;
    protected handleKeyValue: (s: string) => void;
    protected handleStringValue(s: string): void;
    protected handleStartNumber: () => void;
    protected handleNumberChunk: (s: string) => void;
    protected handleEndNumber: () => void;
    protected handleNullValue: () => void;
    protected handleTrueValue: () => void;
    protected handleFalseValue: () => void;
    equals: (other: any) => boolean;
    hashCode: () => number;
}
