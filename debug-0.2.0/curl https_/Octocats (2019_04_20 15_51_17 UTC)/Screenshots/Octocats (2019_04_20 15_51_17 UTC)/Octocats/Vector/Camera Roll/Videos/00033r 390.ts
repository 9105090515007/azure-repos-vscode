import { CompressedJSON } from "./CompressedJSON";
import { StringInput } from "../support/Support";
import { TypeBuilder } from "../TypeBuilder";
import { TargetLanguage } from "../TargetLanguage";
import { RunContext } from "../Run";
export interface Input<T> {
    readonly kind: string;
    readonly needIR: boolean;
    readonly needSchemaProcessing: boolean;
    addSource(source: T): Promise<void>;
    finishAddingInputs(): Promise<void>;
    singleStringSchemaSource(): string | undefined;
    addTypes(ctx: RunContext, typeBuilder: TypeBuilder, inferMaps: boolean, inferEnums: boolean, fixedTopLevels: boolean): Promise<void>;
}
export interface JSONSourceData {
    name: string;
    samples: StringInput[];
    description?: string;
}
export declare class JSONInput implements Input<JSONSourceData> {
    private readonly _compressedJSON;
    readonly kind: string;
    readonly needIR: boolean;
    readonly needSchemaProcessing: boolean;
    private readonly _topLevels;
    constructor(_compressedJSON: CompressedJSON);
    private addSample;
    private setDescription;
    addSource(source: JSONSourceData): Promise<void>;
    finishAddingInputs(): Promise<void>;
    singleStringSchemaSource(): undefined;
    addTypes(_ctx: RunContext, typeBuilder: TypeBuilder, inferMaps: boolean, inferEnums: boolean, fixedTopLevels: boolean): Promise<void>;
}
export declare function jsonInputForTargetLanguage(targetLanguage: string | TargetLanguage, languages?: TargetLanguage[]): JSONInput;
export declare class InputData {
    private _inputs;
    addInput<T>(input: Input<T>): void;
    addSource<T>(kind: string, source: T, makeInput: () => Input<T>): Promise<void>;
    finishAddingInputs(): Promise<void>;
    addTypes(ctx: RunContext, typeBuilder: TypeBuilder, inferMaps: boolean, inferEnums: boolean, fixedTopLevels: boolean): Promise<void>;
    readonly needIR: boolean;
    readonly needSchemaProcessing: boolean;
    singleStringSchemaSource(): string | undefined;
}
