import { ConvenienceRenderer, ForbiddenWordsInfo } from "../ConvenienceRenderer";
import { Name, Namer } from "../Naming";
import { EnumOption, Option, StringOption, OptionValues } from "../RendererOptions";
import { TargetLanguage } from "../TargetLanguage";
import { EnumType, ObjectType, UnionType } from "../Type";
import { RenderContext } from "../Renderer";
export declare enum Framework {
    None = 0,
    Klaxon = 1
}
export declare const kotlinOptions: {
    framework: EnumOption<Framework>;
    packageName: StringOption;
};
export declare class KotlinTargetLanguage extends TargetLanguage {
    constructor();
    protected getOptions(): Option<any>[];
    readonly supportsOptionalClassProperties: boolean;
    readonly supportsUnionsWithBothNumberTypes: boolean;
    protected makeRenderer(renderContext: RenderContext, untypedOptionValues: {
        [name: string]: any;
    }): ConvenienceRenderer;
}
export declare class KotlinRenderer extends ConvenienceRenderer {
    private readonly _kotlinOptions;
    constructor(targetLanguage: TargetLanguage, renderContext: RenderContext, _kotlinOptions: OptionValues<typeof kotlinOptions>);
    readonly _justTypes: boolean;
    protected forbiddenNamesForGlobalNamespace(): string[];
    protected forbiddenForObjectProperties(_o: ObjectType, _classNamed: Name): ForbiddenWordsInfo;
    protected forbiddenForEnumCases(_e: EnumType, _enumName: Name): ForbiddenWordsInfo;
    protected forbiddenForUnionMembers(_u: UnionType, _unionName: Name): ForbiddenWordsInfo;
    protected topLevelNameStyle(rawName: string): string;
    protected makeNamedTypeNamer(): Namer;
    protected namerForObjectProperty(): Namer;
    protected makeUnionMemberNamer(): Namer;
    protected makeEnumCaseNamer(): Namer;
    protected emitDescriptionBlock(lines: string[]): void;
    private emitBlock;
    private kotlinType;
    private unionMemberFromJsonValue;
    private unionMemberJsonValueGuard;
    private emitHeader;
    private emitTopLevelArray;
    private emitTopLevelMap;
    private klaxonRenameAttribute;
    private emitEmptyClassDefinition;
    private emitClassDefinition;
    private emitGenericConverter;
    private emitEnumDefinition;
    private emitUnionDefinition;
    protected emitSourceStructure(): void;
}
