import { TargetLanguage } from "../TargetLanguage";
import { Type, ClassType, EnumType, ClassProperty } from "../Type";
import { Name, Namer } from "../Naming";
import { Sourcelike } from "../Source";
import { ConvenienceRenderer, ForbiddenWordsInfo } from "../ConvenienceRenderer";
import { StringOption, BooleanOption, EnumOption, Option, OptionValues } from "../RendererOptions";
import { RenderContext } from "../Renderer";
export declare type OutputFeatures = {
    interface: boolean;
    implementation: boolean;
};
export declare const objcOptions: {
    features: EnumOption<{
        interface: boolean;
        implementation: boolean;
    }>;
    justTypes: BooleanOption;
    marshallingFunctions: BooleanOption;
    classPrefix: StringOption;
    extraComments: BooleanOption;
};
export declare class ObjectiveCTargetLanguage extends TargetLanguage {
    constructor();
    protected getOptions(): Option<any>[];
    protected makeRenderer(renderContext: RenderContext, untypedOptionValues: {
        [name: string]: any;
    }): ObjectiveCRenderer;
}
export declare class ObjectiveCRenderer extends ConvenienceRenderer {
    private readonly _options;
    private _currentFilename;
    private readonly _classPrefix;
    constructor(targetLanguage: TargetLanguage, renderContext: RenderContext, _options: OptionValues<typeof objcOptions>);
    private inferClassPrefix;
    protected forbiddenNamesForGlobalNamespace(): string[];
    protected forbiddenForObjectProperties(_c: ClassType, _className: Name): ForbiddenWordsInfo;
    protected forbiddenForEnumCases(_e: EnumType, _enumName: Name): ForbiddenWordsInfo;
    protected makeNamedTypeNamer(): Namer;
    protected namerForObjectProperty(_: ClassType, p: ClassProperty): Namer;
    protected makeUnionMemberNamer(): null;
    protected makeEnumCaseNamer(): Namer;
    protected namedTypeToNameForTopLevel(type: Type): Type | undefined;
    protected emitDescriptionBlock(lines: string[]): void;
    private emitBlock;
    private emitMethod;
    private emitExtraComments;
    protected startFile(basename: Sourcelike, extension: string): void;
    protected finishFile(): void;
    private memoryAttribute;
    private objcType;
    private jsonType;
    private fromDynamicExpression;
    private toDynamicExpression;
    private implicitlyConvertsFromJSON;
    private implicitlyConvertsToJSON;
    private emitPropertyAssignment;
    private emitPrivateClassInterface;
    private pointerAwareTypeName;
    private emitNonClassTopLevelTypedef;
    private topLevelFromDataPrototype;
    private topLevelFromJSONPrototype;
    private topLevelToDataPrototype;
    private topLevelToJSONPrototype;
    private emitTopLevelFunctionDeclarations;
    private emitTryCatchAsError;
    private emitTopLevelFunctions;
    private emitClassInterface;
    private emitClassImplementation;
    private emitMark;
    private variableNameForTopLevel;
    private emitPseudoEnumInterface;
    private emitPseudoEnumImplementation;
    protected emitSourceStructure(proposedFilename: string): void;
    private readonly needsMap;
    private emitMapFunction;
}
