import { TargetLanguage } from "../TargetLanguage";
import { Type, ClassType, EnumType, UnionType, TypeKind, ClassProperty } from "../Type";
import { Name, Namer } from "../Naming";
import { BooleanOption, EnumOption, Option, StringOption, OptionValues } from "../RendererOptions";
import { Sourcelike } from "../Source";
import { ConvenienceRenderer, ForbiddenWordsInfo } from "../ConvenienceRenderer";
import { RenderContext } from "../Renderer";
import { StringTypeMapping } from "../TypeBuilder";
import { DateTimeRecognizer } from "../DateTime";
export declare const swiftOptions: {
    justTypes: BooleanOption;
    convenienceInitializers: BooleanOption;
    urlSession: BooleanOption;
    alamofire: BooleanOption;
    namedTypePrefix: StringOption;
    useClasses: EnumOption<boolean>;
    dense: EnumOption<boolean>;
    linux: BooleanOption;
    accessLevel: EnumOption<string>;
    protocol: EnumOption<{
        equatable: boolean;
        hashable: boolean;
    }>;
};
export declare class SwiftTargetLanguage extends TargetLanguage {
    constructor();
    protected getOptions(): Option<any>[];
    readonly stringTypeMapping: StringTypeMapping;
    readonly supportsOptionalClassProperties: boolean;
    readonly supportsUnionsWithBothNumberTypes: boolean;
    protected makeRenderer(renderContext: RenderContext, untypedOptionValues: {
        [name: string]: any;
    }): SwiftRenderer;
    readonly dateTimeRecognizer: DateTimeRecognizer;
}
export declare class SwiftRenderer extends ConvenienceRenderer {
    private readonly _options;
    private _needAny;
    private _needNull;
    constructor(targetLanguage: TargetLanguage, renderContext: RenderContext, _options: OptionValues<typeof swiftOptions>);
    protected forbiddenNamesForGlobalNamespace(): string[];
    protected forbiddenForObjectProperties(_c: ClassType, _classNamed: Name): ForbiddenWordsInfo;
    protected forbiddenForEnumCases(_e: EnumType, _enumName: Name): ForbiddenWordsInfo;
    protected forbiddenForUnionMembers(_u: UnionType, _unionName: Name): ForbiddenWordsInfo;
    protected makeNamedTypeNamer(): Namer;
    protected namerForObjectProperty(): Namer;
    protected makeUnionMemberNamer(): Namer;
    protected makeEnumCaseNamer(): Namer;
    protected isImplicitCycleBreaker(t: Type): boolean;
    protected emitDescriptionBlock(lines: string[]): void;
    private emitBlock;
    private emitBlockWithAccess;
    private justTypesCase;
    protected swiftPropertyType(p: ClassProperty): Sourcelike;
    protected swiftType(t: Type, withIssues?: boolean, noOptional?: boolean): Sourcelike;
    protected proposedUnionMemberNameForTypeKind: (kind: TypeKind) => string | null;
    private renderHeader;
    private renderTopLevelAlias;
    private getProtocolString;
    private getEnumPropertyGroups;
    private readonly accessLevel;
    private renderClassDefinition;
    private emitNewEncoderDecoder;
    private emitConvenienceInitializersExtension;
    private renderEnumDefinition;
    private renderUnionDefinition;
    private emitTopLevelMapAndArrayConvenienceInitializerExtensions;
    private emitDecodingError;
    private emitSupportFunctions4;
    private emitConvenienceMutator;
    protected emitMark(line: Sourcelike, horizontalLine?: boolean): void;
    protected emitSourceStructure(): void;
    private emitURLSessionExtension;
    private emitAlamofireExtension;
}
